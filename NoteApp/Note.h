//
//  Note.h
//  NoteApp
//
//  Created by Vincent on 2016/10/17.
//  Copyright © 2016年 MAPD. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <UIKit/UIKit.h>
@import UIKit;
@import CoreData;

//@interface Note : NSObject<NSCoding>
@interface Note : NSManagedObject

@property(nonatomic) NSString *text;
//@property(nonatomic) UIImage *image;
@property(nonatomic) NSString *imageName; //放圖檔的檔名
-(UIImage*)image; //NShomeDirectory + Documents + 檔名，取得圖檔

-(UIImage*) thumnailImage;

@end










