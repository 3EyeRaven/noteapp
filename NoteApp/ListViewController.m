//
//  ListViewController.m
//  NoteApp
//
//  Created by Vincent on 2016/10/14.
//  Copyright © 2016年 MAPD. All rights reserved.
//

#import "ListViewController.h"
#import "Note.h"
#import "NoteCell.h"
#import "NoteViewController.h"
#import "CoreDataHelper.h"

@interface ListViewController ()<UITableViewDataSource,UITableViewDelegate,NoteViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableTopConstraint;
@property(nonatomic) NSMutableArray *notes;
@end

@implementation ListViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.notes = [NSMutableArray array];
        //[self loadFromFile];
        
        [self queryFromCoreData];
        
        
        [[NSNotificationCenter defaultCenter]addObserver:self
    selector:@selector(finishUpdate:) name:@"NoteUpdated"
        object:nil];
        
    }
    return self;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark NSNotifcation
-(void)finishUpdate:(NSNotification*)notification{
    
    Note *note = notification.userInfo[@"note"];
    
    //找到array中相對應的index
    NSUInteger index = [self.notes indexOfObject:note];
    //組成NSIndxPath物件
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    //tableView 重畫該位置的cell
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    //自動計算高度
    self.tableView.estimatedRowHeight = 44;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
}

//-(void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//    [self.tableView reloadData];
//}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    
    [self.tableView setEditing:editing animated:YES];
    
}

#pragma mark Archiving
-(void)saveToFile{
    
    NSString *docPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *filePath = [docPath stringByAppendingPathComponent:@"notes.archive"];
    
    [NSKeyedArchiver archiveRootObject:self.notes toFile:filePath];
    
}
-(void)loadFromFile{
    NSString *docPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *filePath = [docPath stringByAppendingPathComponent:@"notes.archive"];
    NSArray *data = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    self.notes = [NSMutableArray arrayWithArray:data];//轉成是mutable型式
    
}
#pragma mark Core Data
-(void)queryFromCoreData{
    
    CoreDataHelper *helper = [CoreDataHelper sharedInstance];
    //查詢物件
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Note"];
    
    //利用objectcontext進行查詢
    //[helper.managedObjectContext performBlockAndWait:^{
        
        NSError *error = nil;
        NSArray *results =  [helper.managedObjectContext executeFetchRequest:request error:&error];
        
        if ( error ){
            NSLog(@"error while query %@",error);
            self.notes = [NSMutableArray array];
        }else{
            self.notes = [NSMutableArray arrayWithArray:results];
        }
        
    //}];
    
}
-(void)saveToCoreData{
    
    CoreDataHelper *helper = [CoreDataHelper sharedInstance];

    NSError *error = nil;
    
    [helper.managedObjectContext save:&error];
    
    if ( error ){
        NSLog(@"error saving %@",error);
    }

}


#pragma mark IBAction
- (IBAction)edit:(id)sender {
    
    self.tableView.editing = !self.tableView.editing;
    
    //[self.tableView setEditing:!self.tableView.editing animated:YES];
    
}
- (IBAction)upload:(id)sender {
    
    UIBarButtonItem *item = sender;
    UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    item.customView = indicatorView;
    
    [indicatorView startAnimating];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
        
    });

    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //執行３秒
        [NSThread sleepForTimeInterval:3];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            item.customView = nil;
        });
        
    });
    
}

- (IBAction)addNote:(id)sender {
    
    //Note *note = [[Note alloc]init];
    CoreDataHelper *helper = [CoreDataHelper sharedInstance];
    
    Note *note = [NSEntityDescription insertNewObjectForEntityForName:@"Note" inManagedObjectContext:helper.managedObjectContext];
    
    note.text = @"new note";
    [self.notes insertObject:note atIndex:0];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    //[self saveToFile];
    [self saveToCoreData];
    
}




#pragma mark UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   //第幾個section有幾筆資料
    return self.notes.count;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //custom cell
    //NoteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customCell" forIndexPath:indexPath];
    //cell.nameLabel.text = note.text;

    
    //Reuse , tableview自動會幫你作reuse
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"noteCell" forIndexPath:indexPath];
    
    cell.showsReorderControl = YES;
    Note *note = self.notes[indexPath.row];
    cell.textLabel.text = note.text;
    cell.imageView.image = [note thumnailImage];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ( editingStyle == UITableViewCellEditingStyleDelete ){
        
        CoreDataHelper *helper = [CoreDataHelper sharedInstance];

        Note *note = self.notes[indexPath.row];
        [helper.managedObjectContext deleteObject:note];
        [self saveToCoreData];//偷懶的作法
        
        [self.notes removeObjectAtIndex:indexPath.row];
        
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self saveToFile];
        
    }
    
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    
    Note *note = self.notes[sourceIndexPath.row];
    [self.notes removeObjectAtIndex:sourceIndexPath.row];
    [self.notes insertObject:note atIndex:destinationIndexPath.row];
    
    
}
#pragma mark UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"點選到%ld筆",indexPath.row);
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    /*
    NoteViewController *noteViewController =  [self.storyboard instantiateViewControllerWithIdentifier:@"noteViewController"];
    [self.navigationController pushViewController:noteViewController animated:YES];
    */
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ( [segue.identifier isEqualToString:@"noteSegue"] ){
        //往note view controller
        NoteViewController *noteViewController =  segue.destinationViewController;
        
        //取得使用者點選的indexPath
        NSIndexPath *indexPath = self.tableView.indexPathForSelectedRow;
        Note *note = self.notes[indexPath.row]; //取得該位置的note物件
        
        noteViewController.currentNote = note;//傳到下一個view controller
        noteViewController.delegate = self;
        
    }
}
#pragma mark NoteViewControllerDelegate
-(void)didFinishUpdateNote:(Note*)note{
    //找到array中相對應的index
    NSUInteger index = [self.notes indexOfObject:note];
    //組成NSIndxPath物件
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    //tableView 重畫該位置的cell
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    //[self saveToFile];
    [self saveToCoreData];
}


@end





