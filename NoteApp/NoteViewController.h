//
//  NoteViewController.h
//  NoteApp
//
//  Created by Vincent on 2016/10/17.
//  Copyright © 2016年 MAPD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Note.h"
#import "ListViewController.h"

@protocol NoteViewControllerDelegate <NSObject>
@optional
-(void)didFinishUpdateNote:(Note*)note;
@end
@interface NoteViewController : UIViewController
@property(nonatomic) Note *currentNote;
//任何物件，只要有實作NoteViewControllerDelegate，
//也就是説，只要這個物件有didFinishUpdateNote：就可以被通知
@property(nonatomic,weak) id<NoteViewControllerDelegate> delegate;
@end














