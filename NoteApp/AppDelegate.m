//
//  AppDelegate.m
//  NoteApp
//
//  Created by Vincent on 2016/10/14.
//  Copyright © 2016年 MAPD. All rights reserved.
//

#import "AppDelegate.h"

#import "Reachability.h"
@import SystemConfiguration;

@interface AppDelegate ()
@property(nonatomic) Reachability *reachability;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    NSLog(@"home=%@",NSHomeDirectory());
    NSLog(@"home=%@",NSHomeDirectory());

    //使用者是否是第一次安裝
    NSString *key = @"firstInstall";
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [userDefaults boolForKey:key]){
        //使用者安裝過，不做任何處理
    }else{
        //使用者第一次安裝
        //顯示感謝訊息
        NSLog(@"Thanks you");
        [userDefaults setBool:YES forKey:key];
        //強制寫入硬碟
        [userDefaults synchronize];
    }
    
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkChanged) name:kReachabilityChangedNotification object:nil];
    
    [self networkChanged];
    NSLog(@"home=%@",NSHomeDirectory());

    return YES;
}

-(void)networkChanged{
    
    
    if ( [self.reachability currentReachabilityStatus] == ReachableViaWiFi ){
        NSLog(@"wifi");
    }else if ( [self.reachability currentReachabilityStatus] == ReachableViaWWAN ){
        NSLog(@"3G");
    }else if ( [self.reachability currentReachabilityStatus]== NotReachable){
        NSLog(@"no internet connection");
    }
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
