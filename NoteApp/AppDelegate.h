//
//  AppDelegate.h
//  NoteApp
//
//  Created by Vincent on 2016/10/14.
//  Copyright © 2016年 MAPD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

