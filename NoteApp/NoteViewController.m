//
//  NoteViewController.m
//  NoteApp
//
//  Created by Vincent on 2016/10/17.
//  Copyright © 2016年 MAPD. All rights reserved.
//

#import "NoteViewController.h"

@interface NoteViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property(nonatomic) BOOL isNewImage;
@property(nonatomic) NSLayoutConstraint *ratioConstraint;
@end

@implementation NoteViewController

-(void)dealloc{
    NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //初始化畫面上的資料
    self.textView.text = self.currentNote.text;
    self.imageView.image = self.currentNote.image;
    
    self.imageView.layer.borderWidth = 10;
    self.imageView.layer.borderColor = [UIColor orangeColor].CGColor;
    
    
    NSLog(@"toolbar size %@",NSStringFromCGSize(self.toolbar.intrinsicContentSize));
    
    NSLog(@"imageView size %@",NSStringFromCGSize(self.imageView.intrinsicContentSize));
    
    NSLog(@"image size %@",NSStringFromCGSize(self.imageView.image.size));
    
    NSLayoutConstraint *ratioConstraint = [self.imageView.heightAnchor constraintEqualToAnchor:self.imageView.widthAnchor multiplier:0.75];
    
    UIScreen *screen = [UIScreen mainScreen];
    
    [self.imageView.heightAnchor constraintEqualToConstant:screen.bounds.size.height*0.3].active = YES;
    
    if ( self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassRegular){
    
        ratioConstraint.active = YES;
    }
    
    self.ratioConstraint = ratioConstraint;
    
    
    
    /*
    NSArray *hToolbars = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[toolbar]|" options:0 metrics:0 views:@{@"toolbar":self.toolbar}];
    NSArray *vToolbars = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[toolbar]|" options:0 metrics:0 views:@{@"toolbar":self.toolbar}];
    
    [NSLayoutConstraint activateConstraints:hToolbars];
    [NSLayoutConstraint activateConstraints:vToolbars];
    
    */
    
    
}

-(void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    
    [super willTransitionToTraitCollection:newCollection withTransitionCoordinator:coordinator];
    
    if ( newCollection.verticalSizeClass == UIUserInterfaceSizeClassCompact){
        // hC 橫向
        //不要4:3
        self.ratioConstraint.active = NO;
        
    }else{
        //要4:3
        self.ratioConstraint.active = YES;
    }
    [self.toolbar invalidateIntrinsicContentSize];
}

- (IBAction)camera:(id)sender {
    
    
    //加 NSPhotoLibraryUsageDescription 在 info.plist
    
    UIImagePickerController *pickerCtrl = [[UIImagePickerController alloc] init];
    pickerCtrl.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    pickerCtrl.delegate = self;
    
    [self presentViewController:pickerCtrl animated:YES completion:nil];
    
}
- (IBAction)done:(id)sender {
    
    self.currentNote.text = self.textView.text;
    //self.currentNote.image = self.imageView.image;
    
    if ( self.isNewImage ){
        //轉成檔案
        //產生UUID
        NSUUID *uuid = [NSUUID UUID];
        //檔案名稱= (xxx-xxx.xxx-xxx.jpg)
        NSString *fileName = [NSString stringWithFormat:@"%@.jpg",[uuid UUIDString]];
        NSString *docPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        //欲寫入的檔案路徑
        NSString *filePath = [docPath stringByAppendingPathComponent:fileName];
        //將UIImage轉成jpeg形式的NSData
        NSData *imageData = UIImageJPEGRepresentation(self.imageView.image, 1);
        //將NSData寫入路徑變成檔案
        [imageData writeToFile:filePath atomically:YES];
        //將檔名放入note物件中
        self.currentNote.imageName = fileName;
    }
    
    
    
    
    if ( [self.delegate respondsToSelector:@selector(didFinishUpdateNote:)]){
        [self.delegate didFinishUpdateNote:self.currentNote];
    }
    
    /*
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NoteUpdated" object:nil userInfo:@{@"note":self.currentNote}];
    */
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    self.imageView.image = image;
    self.isNewImage = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
